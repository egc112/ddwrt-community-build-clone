#!/bin/bash

# This script applies all files in the extras directory with the extension .patch
# So if you do not want a particular patc just add no to the extension

set -x #keep track on what is going on

cd $DDWRT_BASE

for patch in $SRCDIR/extras/*.patch
do
	echo Applying "${patch}"
	patch -p0 < "${patch}"
done

cd $DDWRT_BASE/src/router
