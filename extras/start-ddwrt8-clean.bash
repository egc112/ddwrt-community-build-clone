#!/bin/bash

#NOTE run script IN the shell with . ./start-ddwrt  Note the preceding dot!
# https://devdojo.com/bobbyiliev/how-to-create-an-interactive-menu-in-bash
#SETTINGS
# set -a treat all variables as export
set -a
SRCDIR="/linuxdata/test-4"
DDWRT_BASE="/linuxdata/ddwrt"
GCCARM9_northstar="$SRCDIR/toolchain/arm/openwrt-toolchain-bcm53xx_gcc-9.2.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a9_gcc-9.2.0_musl_eabi/bin"
GCCARM102_northstar="$SRCDIR/toolchain/arm/openwrt-toolchain-bcm53xx-generic_gcc-10.2.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a9_gcc-10.2.0_musl_eabi/bin"
GCCARM103_northstar="$SRCDIR/toolchain/arm/openwrt-toolchain-bcm53xx-generic_gcc-10.3.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a9_gcc-10.3.0_musl_eabi/bin"
GCCARM111_northstar="$SRCDIR/toolchain/arm/openwrt-toolchain-bcm53xx-generic_gcc-11.1.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a9_gcc-11.1.0_musl_eabi/bin"
GCCARM112_northstar="$SRCDIR/toolchain/arm/openwrt-toolchain-bcm53xx-generic_gcc-11.2.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a9_gcc-11.2.0_musl_eabi/bin"
GCCARM9_ipq806x="$SRCDIR/toolchain/ipq806x/openwrt-toolchain-ipq806x-generic_gcc-9.3.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a15+neon-vfpv4_gcc-9.3.0_musl_eabi/bin"
GCCARM102_ipq806x="$SRCDIR/toolchain/ipq806x/openwrt-toolchain-ipq806x-generic_gcc-10.2.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a15+neon-vfpv4_gcc-10.2.0_musl_eabi/bin"
GCCARM103_ipq806x="$SRCDIR/toolchain/ipq806x/openwrt-toolchain-ipq806x-generic_gcc-10.3.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a15+neon-vfpv4_gcc-10.3.0_musl_eabi/bin"
GCCARM111_ipq806x="$SRCDIR/toolchain/ipq806x/openwrt-toolchain-ipq806x-generic_gcc-11.1.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a15+neon-vfpv4_gcc-11.1.0_musl_eabi/bin"
GCCARM112_ipq806x="$SRCDIR/toolchain/ipq806x/openwrt-toolchain-ipq806x-generic_gcc-11.2.0_musl_eabi.Linux-x86_64/toolchain-arm_cortex-a15+neon-vfpv4_gcc-11.2.0_musl_eabi/bin"
CONFIG_PATH="/shrd/ddwrt/configs"

WrongCommand () {
	#do nothing
	menu
}

submenu_platform () {
echo -ne "
Choose a config file to build with
$(ColorYellow '1)') Broadcom Northstar (default)
$(ColorYellow '2)') IPQ806x
$(ColorYellow '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
			"1"|"" )
				echo "You chose Northstar"
				echo 'export DDWRT_PLATFORM="northstar"'
				export DDWRT_PLATFORM="northstar"
				return
				;;
			2 )
				echo "You chose IPQ806x"
				echo 'export DDWRT_PLATFORM="ipq806x"'
				export DDWRT_PLATFORM="ipq806x"
				return
				;;
			0) return 0 ;;
			*) echo -e $red"Wrong option."$clear; WrongCommand;;
        esac
}

# submenu
submenu_toolchain () {
echo -ne "
Choose a config file to build with
$(ColorYellow '1)') Toolchain gcc-11.2.0 (default)
$(ColorYellow '2)') Toolchain gcc-11.1.0
$(ColorYellow '3)') Toolchain gcc-10.3.0
$(ColorYellow '4)') Toolchain gcc-10.2.0
$(ColorYellow '5)') Toolchain gcc-9.x.0
$(ColorYellow '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
			"1"|"" )
				echo "you chose sub item 1"
				var="GCCARM112_${DDWRT_PLATFORM}"
				echo $var
				export GCCARM="${!var}"
				return
				;;
			2 )
				echo "you chose sub item 2"
				var="GCCARM111_${DDWRT_PLATFORM}"
				echo $var
				export GCCARM="${!var}"
				return
				;;
			3 )
				echo "you chose sub item 3"
				var="GCCARM103_${DDWRT_PLATFORM}"
				echo $var
				export GCCARM="${!var}"
				return
				;;
			4 )
				echo "you chose sub item 4"
				var="GCCARM102$_{DDWRT_PLATFORM}"
				echo $var
				export GCCARM="${!var}"
				return
				;;
			5 )
				echo "you chose sub item 5"
				echo $var
				var="GCCARM9_${DDWRT_PLATFORM}"
				export GCCARM="${!var}"
				return
				;;
			0) return 0 ;;
			*) echo -e $red"Wrong option."$clear; WrongCommand;;
        esac
}

submenu_config () {
echo -ne "
Choose a config file to build with
$(ColorYellow '1)') R6400-ksmbd nonmap nomtr (default)
$(ColorYellow '2)') R6400-ksmbd
$(ColorYellow '3)') R6400-Samba36
$(ColorYellow '4)') R7800-ksmbd
$(ColorYellow '5)') EA8500-ksmbd
$(ColorYellow '6)') R7000/Asus Ac68U/EA6900 (use northstar)
$(ColorYellow '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
			"1"|"" )
				echo "you chose item 1"
				BUILD_CONFIG=$CONFIG_PATH/config-r6400-ksmbd-nonmap-mtr
				echo $BUILD_CONFIG
				return
				;;
			2 )
				echo "you chose item 1"
				BUILD_CONFIG=$CONFIG_PATH/config-r6400-ksmbd
				echo $BUILD_CONFIG
				return
				;;
			3 )
				echo "you chose item 2"
				BUILD_CONFIG=$CONFIG_PATH/config-r6400-samba36
				echo $BUILD_CONFIG
				return
				;;
			4 )
				echo "you chose item 3"
				BUILD_CONFIG=$CONFIG_PATH/config-r7800
				echo $BUILD_CONFIG
				return
				;;
			5 )
				echo "you chose item 4"
				BUILD_CONFIG=$CONFIG_PATH/config_ipq806x_ea8500
				echo $BUILD_CONFIG
				return
				;;
			6 )
				echo "you chose item 4"
				BUILD_CONFIG=$CONFIG_PATH/config-ac68u-ksmbd
				echo $BUILD_CONFIG
				return
				;;
			0) return 0 ;;
			*) echo -e $red"Wrong option."$clear; WrongCommand;;
        esac
}


##
# Color  Variables
##
green='\e[92m'
blue='\e[94m'
red='\e[91m'
yellow='\e[93m'
clear='\e[0m'

##
# Color Functions
##

ColorGreen(){
	echo -ne $green$1$clear
}
ColorBlue(){
	echo -ne $blue$1$clear
}
ColorYellow(){
	echo -ne $yellow$1$clear
}
ColorRed(){
	echo -ne $red$1$clear
}

menu(){
echo -ne "
Community build start script
$(ColorGreen '1)') Set Path
$(ColorGreen '2)') Choose Platform
$(ColorGreen '3)') Choose Toolchain
$(ColorGreen '4)') Choose config file
$(ColorGreen '5)') Clean and update tree
$(ColorGreen '6)') Prepare and patch tree
$(ColorGreen '7)') Add local patches
$(ColorGreen '8)') Build
$(ColorGreen '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
			"1"|"" )
				echo "you chose main item 1"
				echo 'export SRCDIR="$SRCDIR"; export DDWRT_BASE="$DDWRT_BASE"'
				export SRCDIR="$SRCDIR"; export DDWRT_BASE="$DDWRT_BASE"
				menu
				;;
			2 )
				echo "you chose main item 2, choose platform"
				submenu_platform
				menu
				;;
			3 )
				echo "you chose main item 3"
				submenu_toolchain
				menu
				;;
			4 )
				echo "you chose main item 4, choose config file"
				submenu_config
				menu
				;;
			5 )
				echo "you chose main item 5"
				echo '$SRCDIR/scripts/update-tree'
				$SRCDIR/scripts/update-tree
				menu
				;;
			6 )
				echo "you chose main item 6"
				echo '$SRCDIR/scripts/prepare-tree'
				$SRCDIR/scripts/prepare-tree
				menu
				;;
			7 )
				echo "you chose main item 7"
				bash -x /shrd/ddwrt/apply-ddwrt-patches-ubuntu20.sh; cd $DDWRT_BASE/src/router
				menu
				;;
			8 )
				echo "you chose main item 8, now building"
				cd $DDWRT_BASE/src/router
				$SRCDIR/scripts/build-ddwrt $BUILD_CONFIG  |& tee build.log
				menu
				;;
			0 ) return 0 ;;
			*) echo -e $red"Wrong option."$clear; WrongCommand;;
        esac
}

# Call the menu function
menu

