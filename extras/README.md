This is the READ.Me file of the extras repository.  

Here you will find extra options/patches for DDWRT, note those are all experimental, do not use them if you do not know how to recover. 

A description of the packages can be found in the [Extras wiki](https://gitlab.com/egc112/ddwrt-community-build-clone/-/wikis/Extras-description-of-packages)
