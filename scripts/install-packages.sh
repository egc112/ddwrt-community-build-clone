#!/bin/bash
# needs cleaning and checking
#Prerequisites:
# ver 28-apr-2024
# for the toolchain from openwrt
sudo apt install -y git
sudo apt install -y gawk
sudo apt install -y libncurses5-dev
# might e needed on older Ubuntu 18.04
#sudo apt install -y rsync

### Newly patched Ubuntu may not yet have the correct kernel headers and might need this.
sudo apt-get install -y linux-headers-$(uname -r)

# for dd-wrt
#for 32 bit:
sudo dpkg --add-architecture i386 && sudo apt-get update

sudo apt install -y subversion
sudo apt install -y autoconf
sudo apt install -y automake automake-1.15
sudo apt install -y libtool
sudo apt install -y ccache
sudo apt install -y bison
sudo apt install -y flex
sudo apt install -y libpcre3-dev # not necessary?  maybe just needed to build pcre before zabbix
#sudo snap install -y cmake --classic # to get the latest version not necessary when using 18.04
sudo apt install -y cmake
sudo apt install -y autopoint
sudo apt install -y sqlite3 # not necessary?  maybe just needed to build minidlna before asterisk
sudo apt install -y sqlite  # not necessary?  maybe just needed to build minidlna before asterisk
sudo apt install -y gperf # for minidlna (frametype.c compat.c will have 0 bytes if gperf missing and wont be regenerated so delete them manually if this happens or get undefined references linking minidlna)
sudo apt install -y uglifyjs # for minifying web UI JS
sudo apt install -y curl
sudo apt install -y libxml2-dev  # for php7
sudo apt install -y gengetopt # for chillispot
sudo apt install -y liblzma-dev # asterisk is not cross compiling during install -y phase
sudo apt install -y zlib1g:i386 # for mkfs.jffs2 during install -y phase (32 bit executable)
sudo apt install -y libstdc++6:i386 # for lzma_4k during install -y phase (32 bit executable)
sudo apt install -y texinfo # fixes a "not found" message during install -y (probably useless)
sudo apt install -y libexpat1-dev # for unbound during configure
sudo apt install -y libssl-dev # for softether
sudo apt install -y libreadline-dev # for softether
sudo apt install -y git-lfs # for downloading tar toolchain
sudo apt install -y build-essential ccache pkg-config squashfs-tools lib32stdc++6
sudo apt install -y python-dev
sudo apt install -y python2.7 python-pip 
sudo apt install -y mtd-utils  
sudo apt install -y libtool-bin  
#For IPQ806X (ubi-fs)
sudo apt install -y zlib1g-dev liblzo2-dev uuid-dev libacl1-dev
# for meson build system glib svn.dd-wrt.com/changeset/46135, mesonbuild.com/Tutorial.html 
sudo apt install -y libgtk2.0-dev   # for glib2
sudo apt install -y python3 python3-pip python3-setuptools python3-wheel
#sudo apt install -y python-is-python3 #will not work need python in /usr/bin/python for error "/usr/bin/env: python: No such file or directory"
sudo apt install -y ninja-build
sudo pip3 install meson  # make sure meson --version is at least 0.60
#sudo pip3 install --user meson
sudo apt install -y libzstd-dev  # for zstd error when compiling with LTO
sudo apt install -y python-docutils  # only necessary for building man pages of OpenVPN after 47853
sudo apt install -y libsodium-dev  # for softether compiling
sudo apt install -y autoconf-archive  # for DBus/avahi
sudo apt install -y libbsd-dev  # for strlcpy in glib.c /usr/include/bds/string.h
# for Ubuntu 22.04
if [[ $(lsb_release -c | grep -c 'jammy') -eq 1  || $(lsb_release -c | grep -c 'noble') -eq 1 ]]; then
	sudo apt install -y libdbus-1-dev
	sudo apt install -y gtk-doc-tools
	sudo apt install -y libglib2.0-dev # to build ksmbd
if [[ $(lsb_release -c | grep -c 'focal') -eq 1 ]]; then
	sudo apt install -y libtirpc-dev #Only for Ubuntu 20.04 if encountering -ltirpc error in building nfs-utils
fi
sudo apt install -y re2c     #might be necessary for Python
# for glib20 after 55109 when error No module named 'packaging' see: https://stackoverflow.com/questions/42222096/no-module-named-packaging
sudo pip3 install --upgrade pip && pip install packaging
