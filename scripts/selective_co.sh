#!/bin/sh

case "$DDWRT_PLATFORM" in
    "northstar")
      KERNEL="4.4"  # On dd-wrt the 1st two numbers of the kernel release "uname -r | cut -d. -f 1-2"
      ;;
    "ipq806x")
      KERNEL="4.9"  # On dd-wrt the 1st two numbers of the kernel release "uname -r | cut -d. -f 1-2"
      ;;
    *)
      echo ""
      echo "ERROR: Please set \$DDWRT_PLATFORM to your router platform, eg. northstar, ipq806x ..."
      echo ""
      exit 1
      ;;
esac

DOWNLOADKERNEL=1

TARGET=${1}
REV=${2}

if [ -z "${TARGET}" ]; then
        echo ${0}: Please specify the target destination
        exit 1
fi

if [ -z "${KERNEL}" ] || [ "${KERNEL}" = "0.0" ]; then
        echo ${0}: Please specify the kernel version on the first line of the script eg. KERNEL=4.9
        exit 1
fi

if [ -z "${REV}" ]; then
    rev_file=$(dirname $0)/DDWRT_REVISION

    if [ ! -f $rev_file ]; then
        echo ""
        echo "ERROR: $rev_file not found"
        echo ""
        exit 1
    fi

    REV=$(cat $rev_file)
fi

if [ ! -z "${REV}" ]; then
    REV="-r r${REV}"
fi

svn co svn://svn.dd-wrt.com/DD-WRT ${REV} "${TARGET}" --depth immediates --quiet

for tmp1 in $( find "${TARGET}" -type d \( ! -iwholename "${TARGET}" \) | grep --invert-match .svn ); do
        echo ${0}: Updating "${tmp1}" ..
        svn up ${REV} "${tmp1}" --set-depth immediates --quiet

        for tmp2 in $( find "${tmp1}" -type d \( ! -iwholename "${tmp1}" \) | grep --invert-match .svn ); do
                if [ "${TARGET}/src/linux" != ${tmp2} ]; then
                        echo ${0}: -- Updating "${tmp2}" ..
                        svn up ${REV} "${tmp2}" --set-depth infinity --quiet
                else
                        echo ${0}: == Updating "${tmp2}" ..
                        svn up ${REV} "${tmp2}" --set-depth immediates --quiet
                fi
        done
done

if [ $DOWNLOADKERNEL -eq 1 ];then
    echo ${0}: -- Updating "${TARGET}/src/linux/universal" ..
    svn up ${REV} "${TARGET}/src/linux/universal" --set-depth immediates --quiet
    echo ${0}: -- Updating "${TARGET}/src/linux/universal/linux-${KERNEL}" ..
    svn up ${REV} "${TARGET}/src/linux/universal/linux-${KERNEL}" --set-depth infinity --quiet
fi
