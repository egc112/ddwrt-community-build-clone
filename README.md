# DDWRT community build

Toolchains are available in a sibling repo, at https://gitlab.com/egc112/ddwrt-community-build-toolchain-clone/-/tree/master  

For PHICOMM K2P (Ralink MT7621 based) see: https://github.com/tsl0922/DD-WRT

This project, to build your own DDWRT build from source, was undertaken not because we are dissatisfied with the regular builds, our main developer Sebastian Gottschall, aka @BrainSlayer, aka BS, does an admirable job and DDWRT cannot exist and thrive without him.

This project was undertaken because we sometimes just need that extra package built in or really wanted to try out some tweaks or other enhancements and also because we think that it strengthens our community and can improve DDWRT if more tech savvy users can try things out and will come aboard.

Building DDWRT from source and getting a fully functional build is not an easy task, some of us have also build OpenWRT, which is very easy compared to building for DDWRT.

It took a concerted effort of this team and a lot of time and frustration before this task was accomplished but it finally works and we wanted to share this with the community

As this is still a work in progress and not everything is tested yet there is no guarantee that everything is working but several users are already using their own made build without problems.  

We welcome all tech savvy users who can help and improve this project, so your contributions are not only more than welcome but needed.

At this moment only building for Northstar routers is available building for Qulacomm/Atheros IPQ806x e.g. Netgear R7800, Linkys EA8500 is a work in porgress, you can make build but the radios are not working yet other things are working.

Information about how to setup and build is detailed in the wiki's

This projects builds on earlier attempts and the work of [others](https://forum.dd-wrt.com/phpBB2/viewtopic.php?t=269372&postdays=0&postorder=asc&start=0) and also [this](https://fkpwolf.net/2015/03/26/ddwrt-miwifi-build.html) to whom we are grateful.

Please note this is only for personal use, you are not allowed to commercialize, redistribute or make public any of its contents 
or the software you have created using the software/tools/contents of this repository. 



